const express = require('express');
const router = express.Router();

const {registration, signIn} = require('../services/authService');
const {asyncWrapper} = require('../utils/apiUtils');
const {registrationValidator} = require('../middlewares/validationMidlleware');

router.post('/register', registrationValidator, asyncWrapper(async (req, res) => {
    const {
        username,
        password
    } = req.body;

    await registration({username, password});

    res.json({message: 'Account created successfully'});
}));

router.post('/login', asyncWrapper(async (req, res) => {
    const {
        username,
        password
    } = req.body;

    const token = await signIn({username, password});

    res.json({jwt_token: token, message: 'Logged in successfully!'});

}));

module.exports = {
    authRouter: router
};