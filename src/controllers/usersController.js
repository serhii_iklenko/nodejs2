const express = require('express');
const router = express.Router();

const {
    getUserById,
    removeUserById,
    changePasswordUserById
} = require('../services/usersService');

const {
    asyncWrapper
} = require('../utils/apiUtils');

router.get('/me', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const user = await getUserById(userId);

    res.json({user});

}));

router.patch('/me', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const {oldPassword, newPassword} = req.body;

    await changePasswordUserById(userId, oldPassword, newPassword);

    res.json({message: "Success"});
}));

router.delete('/me', asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    const user = await removeUserById(userId);

    res.json({message: "Success"});
}));

module.exports = {
    usersRouter: router
}
