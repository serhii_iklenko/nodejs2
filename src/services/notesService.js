const {Note} = require('../models/noteModel');

const getUserNotes = async (userId, notePayLoad) => {
    const {limit, offset} = notePayLoad;
    const notes = await Note.find({userId})
        .limit(parseInt(limit, 10))
        .skip(parseInt(offset, 10));
    return notes;

}

const addNoteToUser = async (userId, notePayload) => {
    if (!!notePayload.completed !== true) {
        notePayload.completed = false;
    }
    const note = new Note({...notePayload, userId});
    await note.save();
}

const getNoteByIdForUser = async (noteId, userId) => {
    const note = await Note.findOne({_id: noteId, userId});
    return note;
}

const updateNoteByIdForUser = async (noteId, userId, data) => {
    await Note.findOneAndUpdate({_id: noteId, userId}, { $set: data});
}

const checkNoteByIdForUser = async (userId, noteId) => {
    const user = await Note.findOne({_id: noteId, userId});
    await Note.findOneAndUpdate({ _id: noteId, userId }, { $set: { completed: !user.completed } });
}


const deleteNoteByIdForUser = async (noteId, userId) => {
    await Note.findOneAndRemove({_id: noteId, userId});
}

module.exports = {
    getUserNotes,
    addNoteToUser,
    getNoteByIdForUser,
    updateNoteByIdForUser,
    checkNoteByIdForUser,
    deleteNoteByIdForUser
};