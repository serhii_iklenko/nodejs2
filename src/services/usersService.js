const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const getUserById = async (userId) => {
    const user = await User.findOne({_id: userId}, '-__v -password');
    return user;
}

const removeUserById = async (userId) => {
    const user = await User.findOneAndRemove({_id: userId});
}

const changePasswordUserById = async (userId, oldPassword, newPassword) => {
    const user = await User.findOne({_id: userId});

    if (!(await bcrypt.compare(oldPassword, user.password))) {
        throw new Error('Invalid password');
    }

    const newPass = await bcrypt.hash(newPassword, 10);

    await User.findOneAndUpdate({_id: userId}, {$set: {password: newPass}});
}

module.exports = {
    getUserById,
    removeUserById,
    changePasswordUserById
}
