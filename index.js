const express = require("express");
const morgan = require("morgan");
const mongoose = require('mongoose');
const index = express();
const envConfig = require('dotenv').config().parsed;

const {usersRouter} = require('./src/controllers/usersController');
const {notesRouter} = require('./src/controllers/notesController');
const {authRouter} = require("./src/controllers/authController");
const {authMiddleware} = require('./src/middlewares/authMiddleware');
const {NodeCourseError} = require('./src/utils/errors');

index.use(express.json());
index.use(morgan("tiny"));

index.use('/api/auth', authRouter);
index.use('/api/users', [authMiddleware], usersRouter);
index.use('/api/notes', [authMiddleware], notesRouter);

index.use((req, res, next) => {
    res.status(404).json({message: 'Not found'})
});

index.use((err, req, res, next) => {
    if (err instanceof NodeCourseError) {
        return res.status(err.status).json({message: err.message});
    }
    res.status(500).json({message: err.message});
})


index.get('/', (req, res) => {
    res.end('Server is working...');
});

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://Haldey:01280042@cluster.fefns.mongodb.net/nodejs?retryWrites=true&w=majority', {
            useNewUrlParser: true, useUnifiedTopology: true
        });

        index.listen(envConfig.DB_PORT);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`)
    }
};

start();